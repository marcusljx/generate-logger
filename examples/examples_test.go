package examples

import (
	"bytes"
	"strings"
	"testing"

	"gitlab.com/marcusljx/generate-logger/examples/singlelogger"

	"gitlab.com/marcusljx/generate-logger/examples/standard2"

	"gitlab.com/marcusljx/generate-logger/examples/standard"
)

//go:generate go install -v ..
//go:generate generate-logger --package=complex -o=./complex/logger.go --prefixes=Info=I,Debug=D,Warning=W,Error=E,Fatal=F,Security=S
//go:generate generate-logger --package=standard -o=./standard/logger.go
//go:generate generate-logger --package=standard2 -o=./standard2/logger.go --no-f --no-ln
//go:generate generate-logger --package=singlelogger -o=./singlelogger/logger.go --prefixes=Single=ABCD
//go:generate generate-logger --package=unexported -o=./unexported/logger.go --no-export
//go:generate generate-logger --package=withinit -o=./withinit/logger.go --default-output="out/.sample.log"

func TestStandard(t *testing.T) {
	buf := &bytes.Buffer{}
	standard.SetOutput(buf)
	standard.Infof("hello world : %d", 1234)
	standard.Errorf("hello world : %d", 1234)
	standard.Warningf("hello world : %d", 1234)

	newChecker(3, buf.String())(t)
}

func TestStandard2(t *testing.T) {
	buf := &bytes.Buffer{}
	standard2.SetOutput(buf)
	standard2.Info("hello world", 1234)
	standard2.Error("hello world", 1234)
	standard2.Warning("hello world", 1234)

	newChecker(3, buf.String())(t)
}

func TestSingleLogger(t *testing.T) {
	buf := &bytes.Buffer{}
	singlelogger.SetOutput(buf)
	singlelogger.Single("hello world", 1234)
	singlelogger.Singlef("hello world : %d", 1234)
	singlelogger.Singleln("hello world", 1234)

	newChecker(3, buf.String())(t)
}

func newChecker(inputLines int, data string) func(*testing.T) {
	return func(t *testing.T) {
		t.Helper()
		lines := strings.Split(data, "\n")
		if len(lines) != inputLines+1 {
			t.Errorf("expected 3 lines, got %d", len(lines))
		}
		if lastLine := lines[len(lines)-1]; lastLine != "" {
			t.Errorf("expected empty last line, got %s", lastLine)
		}
	}
}
