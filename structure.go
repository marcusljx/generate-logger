package main

// Structure is the pipeline structure to be passed into the template
type Structure struct {
	// values
	Package          string
	NameAndPrefixMap map[string]string
	InitSetOutput    string

	// behaviours
	SuppressFMethods  bool
	SuppressLnMethods bool
	NoExport          bool
}
