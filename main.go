package main

import (
	"bytes"
	"fmt"
	"go/format"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
	"text/template"

	"gitlab.com/marcusljx/generate-logger/internal/createfunc"

	"github.com/spf13/cobra"
)

var (
	structure = &Structure{
		Package:           "main",
		SuppressFMethods:  false,
		SuppressLnMethods: false,
		NameAndPrefixMap: map[string]string{
			"Info":    "I",
			"Error":   "E",
			"Warning": "W",
			"Fatal":   "F",
		},
	}
	out string
	buf = &bytes.Buffer{}

	// Cmd is the root operation
	Cmd = &cobra.Command{
		Use:          "generate-logger",
		Short:        "generate stdlib implementations of *log.Logger",
		Long:         "generate stdlib implementations of *log.Logger",
		Args:         cobra.NoArgs,
		SilenceUsage: true,

		PreRunE: func(_ *cobra.Command, _ []string) error {
			return nil
		},

		RunE: func(_ *cobra.Command, _ []string) error {
			t := template.New("logger.gotmpl")
			t.Funcs(map[string]interface{}{
				"toLower":      strings.ToLower,
				"adjustExport": createfunc.AdjustExport(structure.NoExport),
			})

			t = template.Must(t.Parse(string(loggerTemplate)))
			return t.Execute(buf, structure)
		},

		PostRunE: func(_ *cobra.Command, _ []string) error {
			// gofmt
			data, err := format.Source(buf.Bytes())
			if err != nil {
				log.Printf("dump:\n%s", buf.String())
				return fmt.Errorf("format.Source : %v", err)
			}

			// to stdout
			if out == "" {
				fmt.Printf("%s\n", data)
				return nil
			}

			// to file
			if err := os.MkdirAll(filepath.Dir(out), os.ModePerm); err != nil {
				return fmt.Errorf("os.MkdirAll : %v", err)
			}
			if err := ioutil.WriteFile(out, data, os.ModePerm); err != nil {
				return fmt.Errorf("ioutil.WriteFile : %v", err)
			}

			return nil
		},
	}
)

//go:generate gengar --out=./z_templates.go --declare loggerTemplate=./templates/logger.gotmpl

func main() {
	if err := Cmd.Execute(); err != nil {
		if _, err1 := fmt.Fprintln(os.Stderr, err); err1 != nil {
			panic(err1)
		}
	}
}

func init() {
	Cmd.Flags().StringVarP(&out, "out", "o", out, "dump output to file instead")

	Cmd.Flags().StringVar(&structure.Package, "package", "main", "package name of the file")
	Cmd.Flags().StringToStringVar(&structure.NameAndPrefixMap, "prefixes", structure.NameAndPrefixMap, "mapping of types to their prefix (eg. --types=error=E,info=I,debug=D)")
	Cmd.Flags().StringVar(&structure.InitSetOutput, "default-output", structure.InitSetOutput, "generates an init() method that sets the output to the given value")

	Cmd.Flags().BoolVar(&structure.SuppressFMethods, "no-f", structure.SuppressFMethods, "suppresses generation of Xxxf() methods")
	Cmd.Flags().BoolVar(&structure.SuppressLnMethods, "no-ln", structure.SuppressLnMethods, "suppresses generation of Xxxln() methods")
	Cmd.Flags().BoolVar(&structure.NoExport, "no-export", structure.NoExport, "if present, makes the generator emit unexported functions and streams for use within the output package only")
}
