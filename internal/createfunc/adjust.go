package createfunc

import "unicode"

// AdjustExport returns a func(string)string that adjusts a Symbol
// to be exported or unexported, based on the factory input.
func AdjustExport(noExport bool) func(string) string {
	if noExport {
		return func(s string) string {
			if len(s) == 0 {
				return s
			}
			c := []byte(s)
			c[0] = byte(unicode.ToLower(rune(c[0])))
			return string(c)
		}
	}

	return func(s string) string {
		return s
	}
}
